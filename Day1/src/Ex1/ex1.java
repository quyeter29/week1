package Ex1;

import java.util.Scanner;

public class ex1 {
public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//Ex1
		String name = "Quyet Chu Van";
		System.out.println("My full name: " + name);
		System.out.println("=========================================");
		//Ex2 and Ex3
		String [] task = {"Learning","Swimming","Playing","Working","Have Lunch"};
		              
		System.out.println( "soft by ascending");
		for (int i = 0; i<task.length; i++) {
			System.out.println("Task " + i +":" + task[i]);
		}
		System.out.println("=========================================");
		
		System.out.println( "sort by descending");
		for(int i = task.length-1; i >= 0; i--) {
			System.out.println("Task "+i+":"+task[i]);
		}
		System.out.println("=========================================");
		        
	    System.out.println("sort by letter");
		for(int i = 0; i<4; i++) {
			for(int j = i + 1; j<5; j++) {
				if(task[i].compareTo(task[j]) > 0) {
					String temp = task[i];
					task[i] = task[j];
					task[j] = temp;
				}
			}
		}
		for(int i = 0; i<5; i++) {
			System.out.println("Task "+i+": "+task[i]);
		}
		//Ex4
		System.out.println("=============================================");
		System.out.println("Repeat: ");
		int a = sc.nextInt();
		for(int i = 1; i<=a; i++)
		{
			for(int j = 0; j<task.length;j++) {
				System.out.println("Task " + j +":" + task[j]);
			}
		}
	}
}
