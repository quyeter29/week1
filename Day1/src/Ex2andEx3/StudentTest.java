package Ex2andEx3;

import java.util.ArrayList;
import java.util.Scanner;

public class StudentTest {

	static void showMenu() {
		System.out.println("1: Enter Student: ");
		System.out.println("2: Show Student information: ");
		System.out.println("3: Show max and min the point: ");
		System.out.println("4: Search Id student: ");
		System.out.println("5: Exit! ");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Student> studentList = new ArrayList<>();
		Scanner scan = new Scanner(System.in);
		int choose;
		do {
			showMenu();
			System.out.println("Choose: ");
			choose =Integer.parseInt( scan.nextLine());
			
			switch(choose) {
			case 1: 
				int n;
				System.out.println("Enter Student add number: ");
				n = Integer.parseInt(scan.next()); 
				for (int i = 0; i<n ; i++) {
					Student std = new Student();
					std.inputInfo();
					
					studentList.add(std);
				}
				break;
			case 2:
				for (int i = 0; i < studentList.size(); i++) {
					studentList.get(i).showInfo();
				}
				break;
			case 3:
				int minIndex = 0 , maxIndex = 0;
				float minMark, maxMark;
				minMark = studentList.get(0).getMark();
				maxMark = studentList.get(0).getMark();
				
				for (int i = 1; i < studentList.size(); i++) {
					if(studentList.get(i).getMark() < minMark) {
						minIndex = i;
						minMark = studentList.get(i).getMark();
					}
					if(studentList.get(i).getMark() > maxMark) {
						maxIndex = i;
						maxMark = studentList.get(i).getMark();
					}
				}
				 System.out.println("Student the max point");
				 studentList.get(maxIndex).showInfo();
				 
				 System.out.println("Student the min point");
				 studentList.get(minIndex).showInfo();
				break;
			case 4:
				System.out.println("Search Student: ");
				String rollNoSearch = scan.nextLine();
				int count = 0;
				for (Student student : studentList) {
					if(student.getRollNo().equalsIgnoreCase(rollNoSearch)) {
						student.showInfo();
					}
				}
				if(count == 0) {
					System.out.println("Not found!");
				}
				break;
			case 5:
				System.out.println("Good bye!");
				break;
			default:
				System.out.println("Error! Retype !!");
				break;
			}
		}while(choose != 5);
		
	}

}