 package Ex2andEx3;

import java.util.Scanner;

public class Student extends Person {
	String rollNo;
	float mark;
	String email;
	  
	public Student () {
		
	}
	@Override
	public void showInfo() {
		// TODO Auto-generated method stub
		super.showInfo();
		System.out.print("; rollNo: "+rollNo+"; mark:"+mark+"; email: "+email);
		System.err.println("");
	}
	
	@Override
	@SuppressWarnings("empty-statement")
	public void inputInfo() {
		// TODO Auto-generated method stub
		super.inputInfo();
		
		Scanner input = new Scanner (System.in);
		System.out.println("Enter id student: ");
		while(true) {
			String rollNoInput = input.nextLine();
			boolean check = setRollNo(rollNoInput);
			if(check) {
				break;
			}
		}
		
		System.out.println("Enter student the point: ");
		while(true) {
			float markInput = Float.parseFloat(input.nextLine());
			boolean check = setMark(markInput);
			if(check) {
				break;
			}
		}
		System.out.println("Enter student email: ");
		while(true) {
			String EmailInput = input.nextLine();
			boolean check = setEmail(EmailInput);
			if(check) {
				break;
			}
		}
	}

	public String getRollNo() {
		return rollNo;
	}

	public boolean setRollNo(String rollNo) {
		if (rollNo != null && rollNo.length() == 8) {
			this.rollNo = rollNo;
			return true;
		}else {
			System.err.println("Error! Retype id student (long : 8 characters): ");
			return false;
		}
		
	}

	public float getMark() {
		return mark;
	}

	public boolean setMark(float mark) {
		if(mark >= 0 && mark <= 10) {
			this.mark = mark;
			return true;
		}else {
			System.err.println("Error! Retype the point (The point >= 0 && The point < 10) : ");
			return false;
		}
	}

	public String getEmail() {
		return email;
	}

	public boolean setEmail(String email) {
		if(email != null && email.contains("@") && !email.contains(" ")) {
			this.email = email;
			return true;
		}else {
			System.err.println("Error! Retype email: ");
			return false;
		}
	}
	
	
}
